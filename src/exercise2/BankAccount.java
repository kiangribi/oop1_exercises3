package exercise2;

public class BankAccount {
    final long accountNumber;
    private int accountBalance;

    public BankAccount(long accountNumber) {
        this.accountNumber = accountNumber;
        this.accountBalance = 0;
    }

    boolean deposit(int amount) {
        if (amount <= 0) return false;
        this.accountBalance += amount;
        return true;
    }

    boolean withdraw(int amount)  {
        if (amount > this.accountBalance) return false;
        this.accountBalance -= amount;
        return true;
    }

    int getBalance() {
        return this.accountBalance;
    }
}
