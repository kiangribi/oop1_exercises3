package exercise2;

public class Main {
    public static void main(String[] args) {
        BankAccount acc1 = new BankAccount(123);
        printBalance(acc1);

        acc1.deposit(100);
        printBalance(acc1);

        acc1.withdraw(200);
        printBalance(acc1);

        acc1.withdraw(90);
        printBalance(acc1);

        // Not possible anymore
        //System.out.println(acc1.accountBalance);
    }

    public static void printBalance(BankAccount acc) {
        System.out.printf("[%d] Balance: %d CHF\n", acc.accountNumber, acc.getBalance());
    }
}
