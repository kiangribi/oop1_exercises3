package exercise3;

public class BankCustomer {
    String firstName;
    String lastName;
    String address;
    int age;

    BankManager manager;

    public BankCustomer(String firstName, String lastName, BankManager manager) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.manager = manager;
    }
}
