package exercise3;

public class BankAccount {
    final long accountNumber;
    private int accountBalance;

    BankCustomer customer;

    public BankAccount(long accountNumber, BankCustomer customer) {
        this.accountNumber = accountNumber;
        this.accountBalance = 0;
        this.customer = customer;
    }

    boolean deposit(int amount) {
        if (amount <= 0) return false;
        this.accountBalance += amount;
        return true;
    }

    boolean withdraw(int amount)  {
        if (amount > this.accountBalance) return false;
        this.accountBalance -= amount;
        return true;
    }

    int getBalance() {
        return this.accountBalance;
    }
}
