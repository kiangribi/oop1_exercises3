package exercise5;

public class Rectangle {
    private Point topLeft;
    private Point bottomRight;

    public Rectangle(Point topLeft, Point bottomRight) {
        this.topLeft = topLeft;
        this.bottomRight = bottomRight;
    }

    public Rectangle(Point topLeft, int size) {
        this.topLeft = topLeft;
        this.bottomRight = new Point(
                topLeft.getX() + size, topLeft.getY() + size);
    }

    public Point getTopLeft() {
        return topLeft;
    }

    public Point getBottomRight() {
        return bottomRight;
    }

    boolean isSquare() {
        return (this.bottomRight.getX() - this.topLeft.getX()) ==
                (this.bottomRight.getY() - this.topLeft.getY());
    }


    boolean isSame(Rectangle other) {
        return this.topLeft.isSame(other.topLeft) &&
                this.bottomRight.isSame(other.bottomRight);
    }

    boolean overlaps(Rectangle other) {
        // Not implemented
        return false;
    }

    Rectangle stretch(int factor) {
        this.bottomRight = new Point(
                this.bottomRight.getX() * factor,
                this.bottomRight.getY() * factor);
        return this;
    }

    Rectangle shrink(int factor) {
        this.bottomRight = new Point(
                this.bottomRight.getX() / factor,
                this.bottomRight.getY() / factor);
        return this;
    }
}
