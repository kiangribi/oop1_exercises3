package exercise5;

import java.util.Objects;

public class Point {
    private final int coordinateX;
    private final int coordinateY;

    public Point(int coordinateX, int coordinateY) {
        this.coordinateX = coordinateX;
        this.coordinateY = coordinateY;
    }

    public int getX() {
        return coordinateX;
    }
    public int getY() {
        return coordinateY;
    }

    boolean isSame(Point other) {
        return this.coordinateX == other.coordinateX &&
                this.coordinateY == other.coordinateY;
    }
}
