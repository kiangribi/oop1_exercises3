package exercise1;

public class BankAccount {
    long accountNumber;
    int accountBalance;

    public BankAccount(long accountNumber) {
        this.accountNumber = accountNumber;
        this.accountBalance = 0;
    }

    boolean deposit(int amount) {
        if (amount <= 0) return false;
        this.accountBalance += amount;
        return true;
    }

    boolean withdraw(int amount)  {
        if (amount > this.accountBalance) return false;
        this.accountBalance -= amount;
        return true;
    }
}
