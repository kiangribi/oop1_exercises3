package exercise4;

public class Main {
    public static void main(String[] args) {
        BankManager manager1 = new BankManager("Kate Smith", 1);
        BankManager manager2 = new BankManager("Usw Schnell", 2);

        BankCustomer customer1 = new BankCustomer("Klara", "Meier", manager1);
        BankCustomer customer2 = new BankCustomer("Petra", "Mueller", manager1);
        BankCustomer customer3 = new BankCustomer("Hans", "Meier", manager2);
        BankCustomer customer4 = new BankCustomer("Stefan", "Schmid", manager2);

        BankAccount acc1 = new BankAccount(1, customer1);
        acc1.deposit(12000);
        BankAccount acc2 = new BankAccount(2, customer1);
        acc2.deposit(1000);
        BankAccount acc3 = new BankAccount(3, customer2);
        acc3.deposit(5000);
        BankAccount acc4 = new BankAccount(4, customer3);
        BankAccount acc5 = new BankAccount(5, customer4);
        acc5.deposit(6000);
    }
}
