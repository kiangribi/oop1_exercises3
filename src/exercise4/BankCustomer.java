package exercise4;

public class BankCustomer {
    String firstName;
    String lastName;
    String address;
    int age;

    BankManager manager;

    public BankCustomer(String firstName, String lastName, BankManager manager) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.manager = manager;
    }

    BankAccount openNewAccount(long number) {
        return new BankAccount(number, this);
    }

    public void print() {
        System.out.printf(
                "BankCustomer[firstName=%s, lastName=%s, manager=%s]\n",
                this.firstName, this.lastName, this.manager.toString());
    }
}
