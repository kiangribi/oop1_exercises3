package exercise4;

import java.util.Objects;

public class BankAccount {
    final long accountNumber;
    private int accountBalance;

    BankCustomer customer;

    public BankAccount(long accountNumber, BankCustomer customer) {
        this.accountNumber = accountNumber;
        this.accountBalance = 0;
        this.customer = customer;
    }

    boolean deposit(int amount) {
        if (amount <= 0) return false;
        this.accountBalance += amount;
        return true;
    }

    boolean withdraw(int amount)  {
        if (amount > this.accountBalance) return false;
        this.accountBalance -= amount;
        return true;
    }

    int getBalance() {
        return this.accountBalance;
    }

    boolean sameCustomer(BankAccount other) {
        return Objects.equals(this.customer.firstName, other.customer.firstName) &&
                Objects.equals(this.customer.lastName, other.customer.lastName);
    }

    BankManager getManager() {
        return this.customer.manager;
    }

    public void print() {
        System.out.printf(
                "BankAccount[accountNumber=%d, accountBalance=%d, customer=%s]\n",
                this.accountNumber, this.accountBalance, this.customer.toString());
    }
}
