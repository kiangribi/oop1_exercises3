package exercise4;

public class BankManager {
    String name;
    int managerID;

    public BankManager(String name, int managerID) {
        this.name = name;
        this.managerID = managerID;
    }

    public void print() {
        System.out.printf("BankManager[name=%s, ManagerID=%d]\n", this.name, this.managerID);
    }
}
